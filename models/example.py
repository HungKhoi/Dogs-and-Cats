import matplotlib.pyplot as plt
import os, os.path

def numberOfFiles(DIR):
    return len([name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))])
print("Number of train cats: ", numberOfFiles("./train/Cat") - 1)
print("Number of train dogs: ", numberOfFiles("./train/Dog") - 1)
print("Number of validation cats: ", numberOfFiles("./val/Cat"))
print("Number of validation dogs: ", numberOfFiles("./val/Dog"))
# define location of dataset
folder = "./train/"
category = ['Cat', 'Dog']
# plot first few images
for i in range(len(category)):
    for j in range(5):
        # define subplot
        plt.subplot(2, 5, i * 5 + j + 1)
        # define filename
        filename = str(j) + '.jpg'
        # load image pixels
        image = plt.imread(folder + category[i] + '/' + filename)
        plt.imshow(image)
# show the figure
plt.show()
