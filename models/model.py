import sys
import matplotlib.pyplot as plt
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Dropout
from keras.optimizers import SGD
from keras.preprocessing.image import ImageDataGenerator
from keras.applications.vgg16 import VGG16
from keras.models import Model

# Define model 1 (VGG 1 block)
def define_model1():
    model = Sequential()
    model.add(Conv2D(32, (3, 3), activation = 'relu', kernel_initializer = 'he_uniform', padding = 'same', input_shape = (200, 200, 3)))
    model.add(MaxPooling2D((2, 2)))
    model.add(Flatten())
    model.add(Dense(128, activation = 'relu', kernel_initializer = 'he_uniform'))
    model.add(Dense(1, activation = 'sigmoid'))
    # compile model
    opt = SGD(lr = 0.001, momentum = 0.9)
    model.compile(optimizer = opt, loss = 'binary_crossentropy', metrics = ['accuracy'])
    return model

# Define model 2 (VGG 2 blocks)
def define_model2():
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu', kernel_initializer='he_uniform', input_shape=(200, 200, 3)))
    model.add(MaxPooling2D((2, 2)))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu', kernel_initializer='he_uniform'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Flatten())
    model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
    model.add(Dense(1, activation='sigmoid'))
    # compile model
    opt = SGD(lr = 0.001, momemtum = 0.9)
    model.compile(optimizer = opt, loss = 'binary_crossentropy', metrics = ['accuracy'])
    return model

# Define model 3 (VGG 3 blocks)
def define_model3():
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu', kernel_initializer='he_uniform', input_shape=(200, 200, 3)))
    model.add(MaxPooling2D((2, 2)))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu', kernel_initializer='he_uniform'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu', kernel_initializer='he_uniform'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Flatten())
    model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
    model.add(Dense(1, activation='sigmoid'))
    # compile model
    opt = SGD(lr = 0.001, momemtum = 0.9)
    model.compile(optimizer = opt, loss = 'binary_crossentropy', metrics = ['accuracy'])
    return model

# Define model 4 (VGG 3 blocks with dropout)
def define_model4():
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu', kernel_initializer='he_uniform', input_shape=(200, 200, 3)))
    model.add(MaxPooling2D((2, 2)))
    model.add(Dropout(0.2))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu', kernel_initializer='he_uniform'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Dropout(0.2))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu', kernel_initializer='he_uniform'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Dropout(0.2))
    model.add(Flatten())
    model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))
    # compile model
    opt = SGD(lr = 0.001, momemtum = 0.9)
    model.compile(optimizer = opt, loss = 'binary_crossentropy', metrics = ['accuracy'])
    return model

# Define model 5 (Transfer learning with pre-trained model: VGG16)
def define_model5():
    # load model
    model = VGG16(include_top = False, input_shape= (224, 224, 3))
    # mark loaded layers as not trainable
    for layer in model.layers:
        layer.trainable = False
    # add new classifier layers
    flat = Flatten()(model.layers[-1].output)
    next = Dense(128, activation = 'relu', kernel_initializer = 'he_uniform')(flat)
    output = Dense(1, activation = 'sigmoid')(next)
    # define new model
    model = Model(inputs = model.inputs, outputs = output)
    # compile model
    opt = SGD(lr = 0.001, momentum = 0.9)
    model.compile(optimizer = opt, loss = 'binary_crossentropy', metrics = ['accuracy'])
    return model

# Data generators with only rescale
def data_Generators1():
    return ImageDataGenerator(rescale=1.0/255.0)

# Create data generator to support data augmentation
def data_Generators2():
    return ImageDataGenerator(rescale=1.0/255.0, \
    width_shift_range=0.1, height_shift_range=0.1, horizontal_flip=True)

# Data generator for VGG16
def data_Generators3():
    # create data generator
    datagen = ImageDataGenerator(featurewise_center = True)
    # specify imagenet mean values for centering
    datagen.mean = [123.68, 116.779, 103.939]
    return datagen

# plot diagnostic learning curves
def summarize_diagnostics(history):
	# plot loss
	plt.subplot(211)
	plt.title('Cross Entropy Loss')
	plt.plot(history.history['loss'], color='blue', label='train')
	plt.plot(history.history['val_loss'], color='orange', label='test')
	# plot accuracy
	plt.subplot(212)
	plt.title('Classification Accuracy')
	plt.plot(history.history['accuracy'], color='blue', label='train')
	plt.plot(history.history['val_accuracy'], color='orange', label='test')
	# save plot to file
	filename = sys.argv[0].split('/')[-1]
	plt.savefig(filename + '_plot.png')
	plt.close()
    
# run the test harness for evaluating a model
def run_test_harness2(model, train_datagen, test_datagen):
    # prepare iterators
    train_it = train_datagen.flow_from_directory('./train/', class_mode = 'binary', batch_size = 64, target_size = (224, 224))
    val_it = test_datagen.flow_from_directory('./val/', class_mode = 'binary', batch_size = 64, target_size = (224, 224))
    # fit model
    history = model.fit_generator(train_it, steps_per_epoch = len(train_it), validation_data = val_it, validation_steps = len(val_it), epochs = 10, verbose = 0)
    # evaluate model
    _, acc = model.evaluate_generator(val_it, steps = len(val_it), verbose = 0)
    print("> %.3f" % (acc * 100.0))
    # save model
    model.save("final_model.h5")
    # learning curves
    summarize_diagnostics(history)
    
model = define_model5()
print(model.summary())
