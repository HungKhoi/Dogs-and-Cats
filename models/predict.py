# make a prediction for a new image.
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import os

def load_image(filename):
    # load the image
    img = load_img(filename, target_size=(224, 224))
    # convert to array
    img = img_to_array(img)
    # reshape image
    img = img.reshape(1, 224, 224, 3)
    # center pixel data for vgg16
    img -= [123.68, 116.779, 103.939]
    return img

def predict(filename):
    # load the image
    img = load_image(filename)
    # load model
    model = load_model("final_model.h5")
    # predict class of image
    result = model.predict(img)
    return result[0]

print("Chó" if (predict("./test1/8.jpg") > 0.5) else "Mèo")
# path_dir = "./test1/"
# path_file = "./result.txt"
# f = open(path_file, "w")
# i = 1
# for file in os.listdir(path_dir):
#     if (i == 11):   break
#     src_file = path_dir + file
#     f.write(str(file) + "  " + ("Chó" if (predict(src_file) > 0.5) else "Mèo") + "\n")
#     i += 1
# f.close()
