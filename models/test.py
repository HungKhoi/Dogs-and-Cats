import numpy as np
from sklearn.metrics import pairwise_distances

def compute_distances_no_loops(X_train, X):
    """
    Compute the distance between each test point in X and each training point
    in X_train using no explicit loops.

    Input / Output: Same as compute_distances_two_loops
    """
    num_test = X.shape[0]
    num_train = X_train.shape[0]
    dists = np.zeros((num_test, num_train))
    #########################################################################
    # TODO:                                                                 #
    # Compute the l2 distance between all test points and all training      #
    # points without using any explicit loops, and store the result in      #
    # dists.                                                                #
    #                                                                       #
    # You should implement this function using only basic array operations; #
    # in particular you should not use functions from scipy,                #
    # nor use np.linalg.norm().                                             #
    #                                                                       #
    # HINT: Try to formulate the l2 distance using matrix multiplication    #
    #       and two broadcast sums.                                         #
    #########################################################################
    # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    z_squared = np.sum(X**2, axis=1, keepdims=True) * np.ones((num_test, num_train))
    x_squared = np.sum(X_train**2, axis=1).reshape(1, -1) * np.ones((num_test, num_train))
    zTx = X.dot(X_train.T)
    dicts = np.sqrt(z_squared + x_squared - 2 * zTx)
    return dicts

X_train = np.array([[1, 2, 3], [ 3, 5, 6]])
X_test = np.array([[2, 3, 4], [1, 6, 7], [3, 4, 5]])
print(compute_distances_no_loops(X_train, X_test))
print(pairwise_distances(X_train, X_test))

