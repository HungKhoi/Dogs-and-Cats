import os
import numpy as np
from shutil import copyfile
from pathlib import Path
# create directories
val_dir = "./val/"
labeldirs = ["Cat/", "Dog/"]
for label in labeldirs:
    newdir = val_dir + label
    os.makedirs(newdir, exist_ok=True)
# path of train directory
train_dir = "./train/"
# seed random number generator
np.random.seed(1)
# define ratio of pictures to use for validation 
val_ratio = 0.25
for label in labeldirs:
    src_dir = train_dir + label
    dst_dir = val_dir + label
    for file in os.listdir(src_dir):
        if (np.random.random() >= val_ratio):   continue
        src_file = src_dir + file
        dst_file = dst_dir + file
        copyfile(src_file, dst_file)
        file_path = Path(src_file)
        file_path.unlink()
